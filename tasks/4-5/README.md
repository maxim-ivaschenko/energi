# Description


# Shell way
Linux
```
grep ERROR sample.log | sed 's/\(ERROR\[.\{1,19\}\]\).\+\(err=".\{1,\}"\)/\1 \2/'
```

Mac
```
brew install gnu-sed
grep ERROR sample.log | gsed 's/\(ERROR\[.\{1,19\}\]\).\+\(err=".\{1,\}"\)/\1 \2/'
```

# Python way
Required Python 3.9 (should work for >3.6, but not tested)
```
python3 tasks/4-5/get_err.py
```
