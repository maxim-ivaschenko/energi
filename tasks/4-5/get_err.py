import pathlib
from typing import List

log_path = f"{pathlib.Path(__file__).parent.resolve()}/sample.log"


def get_errors(path: str) -> List[str]:
    with open(log_path, "r") as fh:
        log = [line for line in fh.readlines() if line.startswith("ERROR")]
        return log


def parse_errors(errors: List[str]) -> List[str]:
    parsed = []
    for i in errors:
        ts = i.split(" ")[0]
        details = i.split("err=")[-1].rstrip()
        parsed.append(f"{ts} err={details}")

    return parsed


errors = get_errors(log_path)
errors_parsed = parse_errors(errors)

for err in errors_parsed:
    print(err)
