# Generic vars
variable "name" {
  type        = string
  description = "Name to be used in created resources (may be used as name prefix)"
}

variable "tags" {
  type        = map(string)
  description = "Tags to be added to created resources"
}

# IAM related vars
variable "path" {
  type        = string
  description = "Path to be used in User, Group, Policy"
  default     = "/"
}

variable "users" {
  type        = list(string)
  description = "Users to be created and added to the group"
}
