data "aws_caller_identity" "current" {}

########
# Role #
########
data "aws_iam_policy_document" "this_assume" {
  statement {
    effect = "Allow"

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root",
      ]
    }

    actions = [
      "sts:AssumeRole",
      "sts:TagSession",
    ]
  }
}

resource "aws_iam_role" "this" {
  name               = "${var.name}-ci-role"
  assume_role_policy = data.aws_iam_policy_document.this_assume.json

  tags = merge(
    var.tags,
    { Name = "${var.name}-ci-role" },
  )
}

##########
# Policy #
##########
data "aws_iam_policy_document" "this_policy" {
  statement {
    effect = "Allow"

    condition {
      test     = "StringEquals"
      variable = "aws:PrincipalArn"
      values = [
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user${var.path}$${aws:username}",
      ]
    }

    actions = [
      "sts:AssumeRole",
    ]

    resources = [
      aws_iam_role.this.arn
    ]
  }
}

resource "aws_iam_policy" "this" {
  name        = "${var.name}-ci-policy"
  description = "CI ${var.name} policy"
  path        = var.path
  policy      = data.aws_iam_policy_document.this_policy.json

  tags = merge(
    var.tags,
    { Name = "${var.name}-ci-policy" },
  )
}

#########
# Group #
#########
/*
  This group may be used by some CI service (via access/secret keys, and it might not have MFA enabled).
  This is why we don't add MFA condition to the policy
  ```
    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"
      values = [
        "true",
      ]
    }
  ```
*/
# tfsec:ignore:aws-iam-enforce-mfa
resource "aws_iam_group" "this" {
  name = "${var.name}-ci-group"
  path = var.path
}

resource "aws_iam_group_policy_attachment" "this" {
  group      = aws_iam_group.this.name
  policy_arn = aws_iam_policy.this.arn
}

#########
# Users #
#########
resource "aws_iam_user" "this" {
  for_each = toset(var.users)

  name = each.value
  path = var.path

  tags = merge(
    var.tags,
    { Name = each.value },
  )
}

resource "aws_iam_user_group_membership" "this" {
  for_each = toset(var.users)

  user = aws_iam_user.this[each.value].name

  groups = [
    aws_iam_group.this.name,
  ]
}
