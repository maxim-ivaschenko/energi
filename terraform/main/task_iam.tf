module "stage-ci" {
  source = "../modules/iam"

  name = "stage"
  users = [
    "patrick",
    "joe",
  ]

  tags = {
    env = "stage"
  }
}
