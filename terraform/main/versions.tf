terraform {
  required_version = ">= 1.1.9" # terraform
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.13.0" # aws
    }
  }
}
