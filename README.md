- [Overview](#overview)
  - [Required software](#required-software)
  - [pre-commit hooks](#pre-commit-hooks)
- [Dockerfile (Docker Whale, task #1)](#dockerfile-docker-whale-task-1)
- [K8s STS (Awesomness, task #2)](#k8s-sts-awesomness-task-2)
- [CI (All the continuouses, task #3)](#ci-all-the-continuouses-task-3)
- [Text processing (Script kiddies, Script grown-ups, tasks #4 and #5)](#text-processing-script-kiddies-script-grown-ups-tasks-4-and-5)
  - [Shell way](#shell-way)
  - [Python way](#python-way)
- [Terraform (6 task)](#terraform-6-task)

# Overview
This repo contains solutions to the 6 tasks from the assessment. Some prerequisites needs to be done though
## Required software
* `terraform` v1.9
* [tflint](https://github.com/terraform-linters/tflint) - needed by pre-commit hooks
* [tfsec](https://github.com/aquasecurity/tfsec) - needed by pre-commit hooks
* [terraform-docs](https://github.com/terraform-docs/terraform-docs) - needed by pre-commit hooks

## pre-commit hooks
Several validators were added to the `pre-commit` hooks.\
In order to enable validators, please run:
```
brew install pre-commit
pre-commit install
```

# Dockerfile (Docker Whale, task #1)
* Please find `Dockerfile` by this path: `deploy/Dockerfile`
* GPG/sha256sums was used to verify the release
* Ability to change Energi versions via `VERSION` [Dockerfile arg](https://docs.docker.com/engine/reference/builder/#arg). Default `v3.1.2` was used as defined in the task

# K8s STS (Awesomness, task #2)
* STS was added as a helm chart - `deploy/k8s/helm/charts/energi`
* `helm create` command was used to initialize base helm specs.
* Please see `deploy/k8s/helm/charts/energi/values.yaml` for customizable params.
* 2 envs were added with slightly different params
  * stage - `deploy/k8s/helm/envs/stage/values.yaml`
  * prod - `deploy/k8s/helm/envs/prod/values.yaml`
* Following command to deploy STS with *stage* values
  ```
  helm install --values deploy/k8s/helm/envs/stage/values.yaml  energi deploy/k8s/helm/charts/energi
  ```
* Following command to generate K8s specs with *stage* values
  ```
  helm template --values deploy/k8s/helm/envs/stage/values.yaml  energi --output-dir deploy/k8s/tmp deploy/k8s/helm/charts/energi
  ```

# CI (All the continuouses, task #3)
* GitLab CI was used\
  Although have not had a chance to work with it, it seems to be a good choice for the task. Majority of experience was with Jenkins and Github Actions
* Pipeline definition - `.gitlab-ci.yml`
* "Child pipelines" - `.gitlab-ci/pipelines`
* It looks like that `needs` keyword don't work properly when using [multi-project pipelines (bridge)V](https://docs.gitlab.com/ee/ci/pipelines/multi_project_pipelines.html). Unfortunately had no time to dig dipper. Ordering still works, but fancy connections are not shown in the UI.
* Following jobs/steps are implemented
  * Lint Dockerfile
  * Build Dockerfile
  * Trivy scan resulting image
  * Deploy to *stage*
    * `helm lint` to validate chart
    * `helm template` generation (potential debug purposes)
    * `helm install` output to simulate deploy process
  * Deploy to *prod*
    * `helm lint` to validate chart
    * `helm template` generation (potential debug purposes)
    * `helm install` output to simulate deploy process

# Text processing (Script kiddies, Script grown-ups, tasks #4 and #5)
Let's say we want to check Energi logs for errors in ad-hoc manner and get exact error message only with timestamp. Sample log can be found [here](tasks/4-5/sample.log)

## Shell way
Linux
```
grep ERROR tasks/4-5/sample.log | sed 's/\(ERROR\[.\{1,19\}\]\).\+\(err=".\{1,\}"\)/\1 \2/'
```

Mac, `gnu-sed` should be used for compatibility
```
brew install gnu-sed
grep ERROR tasks/4-5/sample.log | gsed 's/\(ERROR\[.\{1,19\}\]\).\+\(err=".\{1,\}"\)/\1 \2/'
```

## Python way
Required Python 3.9 (should work for >3.6, but not tested)
```
python3 tasks/4-5/get_err.py
```

# Terraform (6 task)
* Please find module in `terraform/modules/iam`
* Module call can be seen in `terraform/main/task_iam.tf`
* Terraform state file was commited to the repo for dem purposes. S3 state should be used
* **IMPORTANT:** `personal` aws cli profile need's to be present in the config
